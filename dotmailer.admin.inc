<?php

/**
 * @file
 * Admin settings include for dotmailer module
 */

define('DOTMAILER_TOKEN_MODULE_URL', 'https://www.drupal.org/project/token');
define('DOTMAILER_TEST_ADDRESS_BOOK_NAME', 'Test');
define('DOTMAILER_DATA_FIELDS_URL', 'https://support.dotmailer.com/entries/51187898-Data-Mapping-Mapping-customer-information-to-dotMailer-contact-data-fields');

/**
 * Global dotmailer settings form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function dotmailer_admin_settings($form, &$form_state) {
  // Subscription success/failure messages
  $form['dotmailer_subscription_success_message'] = [
    '#type' => 'textarea',
    '#title' => t('Subscription Success Message'),
    '#default_value' => variable_get('dotmailer_subscription_success_message', t('Thank you, you have been successfully subscribed.')),
  ];
  $form['dotmailer_subscription_failure_message'] = [
    '#type' => 'textarea',
    '#title' => t('Subscription Failure Message'),
    '#default_value' => variable_get('dotmailer_subscription_failure_message', t('We were unable to subscribe you at this time. Please try again later.')),
  ];
  $form['dotmailer_unsubscription_success_message'] = [
    '#type' => 'textarea',
    '#title' => t('Unsubscription Success Message'),
    '#default_value' => variable_get('dotmailer_unsubscription_success_message', t('Thank you, you have been successfully unsubscribed.')),
  ];
  $form['dotmailer_unsubscription_failure_message'] = [
    '#type' => 'textarea',
    '#title' => t('Unsubscription Failure Message'),
    '#default_value' => variable_get('dotmailer_unsubscription_failure_message', t('We were unable to unsubscribe you at this time. Please try again later.')),
  ];

  // Extra settings
  $form['dotmailer_user_register'] = [
    '#type' => 'checkbox',
    '#title' => t('Show subscription options on the user registration form.'),
    '#description' => t('This will only apply for books granted to the authenticated role.'),
    '#default_value' => variable_get('dotmailer_user_register', FALSE),
  ];
  $form['dotmailer_user_edit'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Subscription Options on User Edit Screen'),
    '#description' => t('If set, a tab will be presented for managing newsletter subscriptions when editing an account.'),
    '#default_value' => variable_get('dotmailer_user_edit', FALSE),
  ];

  return system_settings_form($form);
}

/**
 * Dotmailer API users/accounts form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function dotmailer_admin_accounts($form, &$form_state) {
  if (empty($form_state['num_accts'])) {
    // Do we have any existing accounts?
    $accounts = dotmailer_get_accounts();

    $form_state['num_accts'] = empty($accounts) ? 1 : count($accounts);
  }

  $form['description'] = [
    '#weight' => -1,
    '#markup' => t(
      '<p>Configure dotmailer API users here. See the dotmailer documentation for !setup.</p>',
      [
        '!setup' => l(t('setting up an API user'), 'https://developer.dotmailer.com/docs/getting-started-with-the-api#section-setting-up-your-api-user'),
      ]
    ),
  ];

  $form['dotmailer_account_info'] = [
    '#type' => 'wrapper',
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#weight' => -1,
    '#prefix' => '<div id="accts-fieldset-wrapper">',
    '#suffix' => '</div>',
  ];

  for ($i = 1; $i <= $form_state['num_accts']; $i++) {
    $form['dotmailer_account_info'][$i] = [
      '#type' => 'fieldset',
      '#title' => 'Dotmailer API User ' . $i,
    ];

    // Show the username and password form so we can authenticate the user
    $form['dotmailer_account_info'][$i]['dotmailer_username' . $i] = [
      '#type' => 'textfield',
      '#title' => t('Email address'),
      '#required' => TRUE,
      '#default_value' => variable_get('dotmailer_username' . $i, ''),
      '#description' => t('The email address assigned to the dotmailer API user.'),
    ];
    $form['dotmailer_account_info'][$i]['dotmailer_password' . $i] = [
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#default_value' => variable_get('dotmailer_password' . $i, ''),
      '#description' => t('The password for the dotmailer API user to connect to the dotmailer API.'),
    ];
  }

  if ($form_state['num_accts'] < 5) {
    $form['dotmailer_account_info']['add_more'] = [
      '#type' => 'submit',
      '#value' => t('Add another user'),
      '#submit' => ['dotmailer_add_account'],
      '#ajax' => [
        'callback' => 'dotmailer_accounts_callback',
        'wrapper' => 'accts-fieldset-wrapper',
      ],
    ];
  }

  if ($form_state['num_accts'] > 1) {
    $form['dotmailer_account_info']['remove'] = [
      '#type' => 'submit',
      '#value' => t('Remove user'),
      '#submit' => ['dotmailer_remove_account'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => 'dotmailer_accounts_callback',
        'wrapper' => 'accts-fieldset-wrapper',
      ],
    ];
  }

  $form['actions']['reset'] = [
    '#type' => 'link',
    '#title' => t('Reset all users'),
    '#href' => 'admin/config/dotmailer/accounts/reset',
    '#weight' => 2,
  ];

  $form['#submit'][] = 'dotmailer_admin_accounts_submit';

  return system_settings_form($form);
}

/**
 * Custom submit handler for account settings form.
 *
 * @param array $form
 * @param array $form_state
 */
function dotmailer_admin_accounts_submit(&$form, &$form_state) {
  // Delete data for any unused accounts.
  for ($i = $form_state['num_accts'] + 1; $i <= 5; $i++) {
    variable_del('dotmailer_username' . $i);
    variable_del('dotmailer_password' . $i);
    variable_del('dotmailer_active_lists' . $i);
  }
}

/**
 * Callback for both add and remove buttons.
 *
 * Selects and returns the account details fieldset.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function dotmailer_accounts_callback($form, $form_state) {
  return $form['dotmailer_account_info'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 *
 * @param array $form
 * @param array $form_state
 */
function dotmailer_add_account($form, &$form_state) {
  $form_state['num_accts']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove account" button.
 *
 * Decrements the max counter and causes a rebuild.
 *
 * @param array $form
 * @param array $form_state
 */
function dotmailer_remove_account($form, &$form_state) {
  $form_state['num_accts']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * Dotmailer address book settings form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function dotmailer_admin_books($form, &$form_state) {
  $accounts = dotmailer_get_accounts();

  if (empty($accounts)) {
    drupal_set_message(t('A dotmailer API user is required before address books can be configured.'), 'error');
    drupal_goto('admin/config/dotmailer/accounts');
  }

  $form['description'] = [
    '#markup' => t(
      '<p>On this page, configuration settings for address books that belong to your configured Dotmailer !users can be managed.</p>',
      [
        '!users' => l(t('API users'), '/admin/config/dotmailer/accounts'),
      ]
    ),
  ];

  $address_books = [];
  foreach ($accounts as &$account) {
    $account_address_books = dotmailer_listaddressbooks($account['variable_username'], $account['variable_password']);
    if (empty($account_address_books)) {
      continue;
    }

    foreach ($account_address_books as $account_address_book) {
      $account_address_book->username = $account['variable_username'];
      $account_address_book->password = $account['variable_password'];
    }
    $account['address_books'] = $account_address_books;
    $address_books[$account['username']] = $account_address_books;
  }

  if (empty($address_books)) {
    drupal_set_message(t('You do not have any valid Dotmailer address books.'), 'error');
  }

  $available_lists = [];

  $form['dotmailer_books'] = [
    '#type' => 'vertical_tabs',
    '#collapsible' => FALSE,
    '#title' => 'Dotmailer address books',
    '#tree' => TRUE,
  ];

  $active_address_books = variable_get('dotmailer_active_lists');

  if (!$active_address_books) {
    drupal_set_message(t('You do not have any active Dotmailer address books.'), 'warning');
  }

  foreach ($accounts as $key => $account) {
    $test_address_book_id = NULL;
    foreach ($account['address_books'] as $address_book) {
      // Handle Test address book as it is not possible to write to that address book using the dotmailer API
      if (DOTMAILER_TEST_ADDRESS_BOOK_NAME === $address_book->Name) {
        $test_address_book_id = $address_book->ID;
      }

      $available_lists[$key][$address_book->ID] = t(
        '@title [@id]',
        [
          '@title' => t($address_book->Name),
          '@id' => $address_book->ID,
        ]
      );

      if ($active_address_books[$key - 1] && in_array($address_book->ID,
          $active_address_books[$key - 1])) {
        // Get the stored books
        $form['dotmailer_books']['dotmailer_book_' . $address_book->ID] =
          _dotmailer_admin_settings_address_book_settings(
            $address_book,
            $address_book->username,
            $address_book->password
          );
      }
    }

    // Active list configuration
    $form['dotmailer_active_books' . $key] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 0,
      '#title' => t(
        'Active address books for @apikey',
        ['@apikey' => variable_get($account['variable_username'])]
      ),
    ];

    $form['dotmailer_active_books' . $key]['dotmailer_active_lists' . $key] =
      [
        '#type' => 'checkboxes',
        '#options' => $available_lists[$key],
        '#default_value' => !empty($active_address_books) ? $active_address_books[$key - 1] : [],
        '#multiple' => 'multiple',
        '#description' => t('Select the Dotmailer address books to use within your Drupal site.'),
      ];

    if (isset($test_address_book_id)) {
      // Handle Test Address book as it is not possible to write to that Address book using the dotMailer API
      $form['dotmailer_active_books' . $key]['dotmailer_active_lists' . $key][$test_address_book_id]['#return_value'] = 0;
      $form['dotmailer_active_books' . $key]['dotmailer_active_lists' . $key][$test_address_book_id]['#description'] = t('This address book cannot be used with the Dotmailer API, !api_docs (see ERROR_ADDRESSBOOK_NOTWRITABLE code).', ['!api_docs' => l(t('see documentation'), 'http://www.dotmailer.com/resources/dev-tools-guides/api-error-codes/')]);
      $form['dotmailer_active_books' . $key]['dotmailer_active_lists' . $key][$test_address_book_id]['#disabled'] = TRUE;
    }
  }

  return system_settings_form($form);
}

/**
 * Provides the form elements for the settings for each address book.
 *
 * @param object $address_book
 * @param string $username
 * @param string $password
 *
 * @return array
 */
function _dotmailer_admin_settings_address_book_settings($address_book, $username, $password) {
  static $weight = 0;

  $saved_book = dotmailer_get_saved_address_book($address_book);

  $element = [
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => $address_book->Name,
    '#tree' => TRUE,
  ];

  $element['book_id'] = [
    '#type' => 'value',
    '#value' => $address_book->ID,
  ];

  $element['username'] = [
    '#type' => 'value',
    '#value' => $username,
  ];

  $element['password'] = [
    '#type' => 'value',
    '#value' => $password,
  ];

  $element['name'] = [
    '#type' => 'value',
    '#value' => $address_book->Name,
  ];

  $element['title'] = [
    '#type' => 'textfield',
    '#title' => t('Display title for address book'),
    '#default_value' => isset($saved_book->title) ? $saved_book->title : $address_book->Name,
    '#maxlength' => 127,
  ];

  $element['weight'] = [
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $saved_book->weight ? $saved_book->weight : $weight++,
    '#delta' => 20,
    '#description' => t('Weight of the book in the list, if same as another it will be sorted alphabetically'),
  ];

  $element['description'] = [
    '#type' => 'textarea',
    '#title' => t('Address book description'),
    '#default_value' => $saved_book->description,
    '#description' => t('This description will be shown to the user on the address book sign-up and user account settings pages'),
  ];

  $element['external_url'] = [
    '#type' => 'textfield',
    '#title' => t('URL to view information about this address book'),
    '#default_value' => $saved_book->external_url,
    '#description' => t('Remember to include http://'),
    '#maxlength' => 127,
  ];

  $element['booktype'] = [
    '#type' => 'select',
    '#title' => t('Subscription method'),
    '#options' => [
      DOTMAILER_BOOKTYPE_OPTIN => "Opt-in",
      DOTMAILER_BOOKTYPE_OPTOUT => 'Opt-out',
      DOTMAILER_BOOKTYPE_REQUIRED => 'Required',
    ],
    '#default_value' => isset($saved_book->booktype) ? $saved_book->booktype : DOTMAILER_BOOKTYPE_OPTIN,
    '#description' => t('
      <strong>Opt-in:</strong> Users must sign up to receive messages.<br/>
      <strong>Opt-out: </strong> Users are automatically signed up but may unsubscribe.<br/>
      <strong>Required: </strong> Users will remain on the book as long as they have an account and cannot unsubscribe.
    '),
  ];

  $element['doublein'] = [
    '#type' => 'select',
    '#title' => t('Opt-in method'),
    '#options' => [
      dotmailer::OPTIN_SINGLE => "Single",
      dotmailer::OPTIN_DOUBLE => "Double",
      dotmailer::OPTIN_VERIFIED_DOUBLE => "Verified double",
    ],
    '#default_value' => $saved_book->doublein,
    '#description' => t('
      <strong>Single:</strong> Users are automatically subscribed, no approval needed.<br/>
      <strong>Double: </strong> Users are not automatically subscribed, you must verify their subscription manually.<br/>
      <strong>Verified Double: </strong> Users will be sent a link via email that they must follow to confirm their subscription.
    '),
  ];

  $element['roles'] = [
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t(
      'Choose which roles may subscribe to this address book. All users will see the address books they\'re eligible for at the !subscribe and in the subscription block. Address books assigned to the Authenticated role may also appear in the registration form if that option is selected below. Authenticated user may also manage their book settings from their profile.',
      [
        '!subscribe' => l(t('newsletter subscription page'), 'dotmailer/subscribe'),
      ]
    ),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  foreach (user_roles() as $rid => $name) {
    $element['roles'][$rid] = [
      '#type' => 'checkbox',
      '#title' => $name,
      '#default_value' => isset($saved_book->roles[$rid]) ? $saved_book->roles[$rid] : FALSE,
    ];
  }

  $data_fields = dotmailer_listcontactdatalabels($username, $password);

  if (!$data_fields) {
    return $element;
  }

  $element['datafields'] = [
    '#type' => 'fieldset',
    '#title' => t('Contact Data Fields'),
    '#description' => t(
      'Select Drupal user variables to send to Dotmailer as contact data fields. Available Drupal variables are any profile or !token variables for the given user. For more information on contact data fields, see the !doc.',
      [
        '!doc' => l(t('Dotmailer documentation'), DOTMAILER_DATA_FIELDS_URL),
        '!token' => l(t('token'), DOTMAILER_TOKEN_MODULE_URL),
      ]
    ),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  foreach ($data_fields->ContactDataLabel as $field) {
    $element['datafields'][$field->Name] = [
      '#type' => 'textfield',
      '#title' => $field->Name,
      '#default_value' => !empty($saved_book->datafields[$field->Name]) ? $saved_book->datafields[$field->Name] : '',
    ];
  }

  if (module_exists('token')) {
    $element['datafields']['token_help'] = [
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $element['datafields']['token_help']['help'] = [
      '#theme' => 'token_tree',
      '#token_types' => ['user'],
    ];
  }
  else {
    drupal_set_message(t(
      'Install the !token module to send user values to dotmailer contact data fields',
      ['!token' => l(t('Token'), DOTMAILER_TOKEN_MODULE_URL)]
    ), 'warning');
  }

  return $element;
}

/**
 * Submit the Dotmailer address book settings and serialize the saved books.
 *
 * @param array $form
 * @param array $form_state
 */
function dotmailer_admin_books_validate($form, &$form_state) {
  // No books selected or first time here
  if (empty($form_state['values']['dotmailer_active_lists1'])) {
    return;
  }

  $active_lists = [];

  for ($i = 1; $i <= 5; $i++) {
    if (isset($form_state['values']['dotmailer_active_lists' . $i])) {
      $active_lists[] = $form_state['values']['dotmailer_active_lists' . $i];
    }
  }

  variable_set('dotmailer_active_lists', $active_lists);

  variable_set('dotmailer_user_register', $form_state['values']['dotmailer_user_register']);
  variable_set('dotmailer_user_edit', $form_state['values']['dotmailer_user_edit']);

  unset($form_state['values']['dotmailer_books']['dotmailer_books__active_tab']);

  // Loop through each book and create an array to serialize with,
  // saves storing lots of individual values per book
  $books = [];
  foreach ($form_state['values']['dotmailer_books'] as $form_book) {
    $book = dotmailer_get_saved_address_book($form_book['book_id']);
    $book->Id = $form_book['book_id'];
    $book->Username = $form_book['username'];
    $book->Password = $form_book['password'];
    $book->Name = $form_book['name'];
    $book->title = $form_book['title'];
    $book->weight = $form_book['weight'];
    $book->external_url = $form_book['external_url'];
    $book->roles = array_filter((array) $form_book['roles']);
    $book->description = $form_book['description'];
    $book->booktype = $form_book['booktype'];
    $book->doublein = $form_book['doublein'];
    $book->datafields = $form_book['datafields'];
    $books[] = $book;
  }

  usort($books, function ($a, $b) {
    if ($a->weight == $b->weight) {
      if ($a->Name == $b->Name) {
        return $a->Id < $b->Id ? -1 : 1;
      }

      return $a->Name < $b->Name ? -1 : 1;
    }

    return $a->weight < $b->weight ? -1 : 1;
  });

  foreach ($books as $key => $book) {
    unset($books[$key]);
    $books[$book->Id] = $book;
  }

  // Remove books from the form_state
  unset($form_state['values']['dotmailer_books']);
  variable_set('dotmailer_books', $books);
}

/**
 * Confirm form for resetting the Dotmailer API user/account details.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function dotmailer_admin_reset($form, &$form_state) {
  return confirm_form(
    $form,
    t('Are you sure that you wish to remove all configured Dotmailer API users?'),
    'admin/config/dotmailer/accounts'
  );
}

/**
 * Submit handler for dotmailer_admin_reset().
 */
function dotmailer_admin_reset_submit() {
  for ($i = 1; $i <= 5; $i++) {
    variable_del('dotmailer_username' . $i);
    variable_del('dotmailer_password' . $i);
    variable_del('dotmailer_active_lists' . $i);
  }
  variable_del('dotmailer_active_lists');
  drupal_goto('admin/config/dotmailer/accounts');
}
